- Effettuare il clone del repository dal seguente link https://bitbucket.org/spindox/matteo-fasoli.git
- Accedere al seguente link https://spx-tribe-cloud-devops.signin.aws.amazon.com/console
- Digitare nel campo IAM user name: matteo_fasoli_academy
- Digitare nel campo Password: jJDfij0C$-uJsL0
- Cambiare la region in: Europa (Irlanda) (eu-west-1)
- Creare la VPC e l'istanza EC2 con CloudFormation, al posto della x negli indirizzi IP impostare 6
- Collegarsi all'istanza e avviare minikube, se si ottiene un errore sul servizio docker aspettare un minuto e riprovare ad avviare minikube
- Rendere accessibile la dashboard di minikube da internet sulla porta 8001
	minikube dashboard >/dev/null 2>/dev/null &
	kubectl proxy --address='0.0.0.0' --disable-filter=true >/dev/null 2>/dev/null &
- Per collegarsi alla dashboard inserire l'ip pubblico della macchina, porta 8001 e il path /api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/#/workloads?namespace=default
- Creare i file yaml sulla macchina copiando il contenuto dei file *.yaml presenti al path Server-Container/Manifest K8S
- Creare i container per wordpress e il database mysql
- Rendere accessibile wordpress da internet sulla porta 8080
	kubectl port-forward --address 0.0.0.0 service/wordpress 8080:80 >/dev/null 2>/dev/null &
- Effetuare l'installazione di wordpress da interfaccia web
- Configurare tramite dashboard Cloudwatch di AWS gli allarmi utili
	creare un nuovo pannello di controllo
	impostare come nome del pannello alberico_iannace_academy
	selezionare linea come tipo di widget
	origine parametri
	inserire l'instance ID dell'istanza EC2	creata tramite template CloudFormation (es. i-00c65bbeeffd4ddb8)
	selezionare un parametro relativo alla CPU
- Collegarsi al jenkins al seguente link http://jenkins.cloudevops.it:8080/
- Digitare nel campo username: matteo_fasoli
- Digitare nel campo password: jJDfij0C$-uJsL0
- Eseguire i test descritti nel file istruzioni.txt contenuto nella directory Testing